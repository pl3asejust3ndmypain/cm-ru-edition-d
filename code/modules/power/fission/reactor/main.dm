obj/structure/machinery/power/fission/reactor
    name = "RBMK-4500"
    desc = "Graphite-moderated nuclear power reactor designed by the UPP. The name refers to its unusual design where, instead of a large steel pressure vessel surrounding the entire core, the core is surrounded by a cylindrical steel tank inside a concrete vault and each fuel assembly is enclosed in an individual 8 cm diameter pipe (called a channel) surrounded in graphite which allows the flow of cooling water around the fuel."
    icon = 'icons/obj/structures/machinery/rbmk.dmi'
    icon_state = "reactor_off"
    anchored = 1
    unslashable = TRUE
    unacidable = TRUE

    var/InternalTemperature = 0
    var/CoolantTemperature = 0
    var/HaveCoolant = 0
    var/Reactivity = 0
    var/InternalPressure = 101.5
    var/RodIntegrity = 100
    var/FuelAmount = 0
    var/TimeTillEvent = 0
    var/RodInsertion
    var/Active = 0
    var/Runaway = 0
    var/LidOpened = 0

 obj/structure/machinery/power/fission/reactor/proc/calculate()
    if(Runaway == 1)
        InternalTemperature = InternalTemperature + 100
    Reactivity = FuelAmount * 1.25 - RodInsertion
    sleep(0.25)
    InternalTemperature = InternalTemperature + Reactivity - FuelAmount
    sleep(0.25)
    if(HaveCoolant == 1)
        CoolantTemperature = InternalTemperature / 0.25
        InternalTemperature = CoolantTemperature / 0.25
    if(Temperature > 1000)
        if(Temperature > 1500)
            meltdown()
        else
            icon_state = reactor_overheat
    else
        icon_state = reactor_on
    sleep(2)
    calculate()

obj/structure/machinery/power/fission/reactor/proc/startup()
    //insert sound here
    icon_state = reactor_on
    Active = 1
    //the reactor can't shutdown when already launched

obj/structure/machinery/power/fission/reactor/proc/shutdown()
    //insert sound here
    Active = 0
    FuelAmount = 0
    Reactivity = 0
    RodInsertion = 100

obj/structure/machinery/power/fission/reactor/proc/openlid()
    if(LidOpened == 0)
        //insert opened icon state here
        LidOpened = 1
        if(Active == 1)
            Runaway = 1
    else
        icon_state = reactor_off

obj/structure/machinery/power/fission/reactor/proc/meltdown()
    icon_state  = reactor_meltdown
