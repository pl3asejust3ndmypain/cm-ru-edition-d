obj/structure/machinery/power/fission/control/reactorcontrol
    name = "Control unit BETA"
    desc = "An enormous set of consoles, sensors and buttons, seemingly controlling the reactor"
    //insert icon here
    //insert icon state here
    var/ReactorDestroyed = 0



 obj/structure/machinery/power/fission/control/reactorcontrol/attack_hand(mob/user)
    if(ReactorDestroyed == 1)
        to_chat(user, SPAN_WARNING("The [src] emits several connection failure warnings... Something is VERY wrong!")
        return
	if(!skillcheck(user, SKILL_ENGINEER, SKILL_ENGINEER_ENGI))
		to_chat(user, SPAN_WARNING("You have no idea what these buttons mean!"))
		return
        