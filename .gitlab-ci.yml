image: docker:20.10.2
include:
  - template: 'Workflows/Branch-Pipelines.gitlab-ci.yml'
stages:
  - prepare
  - build
  - extra
default:
  interruptible: true

variables:
  # CI environment settings
  DOCKER_BUILDKIT: 1
  DOCKER_DRIVER: "overlay2"
  DOCKER_TLS_CERTDIR: "/certs"
  DM_PROJECT_NAME: ColonialMarinesALPHA
  # Build targets and deps
  IMG_BUILD_CMBASE: "i386/ubuntu:bionic"
  IMG_BUILD_PYTHON: "python:3.7-buster"
  IMG_BUILD_NODE: "node:15-buster"
  BYOND_MAJOR: "513"
  BYOND_MINOR: "1539"
  SPDMM_VERSION: "suite-1.6"
  RUSTG_VERSION: "0.4.7" # For packaging in runner container
  # Resource settings
  BYOND_DOWNLOAD_URL: "https://secure.byond.com/download/build/${BYOND_MAJOR}/${BYOND_MAJOR}.${BYOND_MINOR}_byond_linux.zip"
  SPDMM_DOWNLOAD_URL: "https://github.com/SpaceManiac/SpacemanDMM/releases/download/${SPDMM_VERSION}"
  # Targets
  BYOND_INSTALL_DIR: "${CI_PROJECT_DIR}/ci-byond-inst"
  # CM_CHANGELOG_DEPLOY_KEY: set to a valid SSH key as GitLab deploy key to push changelog to repo when on main branch
  CM_CHANGELOG_DEPLOY_NAME: "ChangelogBot"
  CM_CHANGELOG_DEPLOY_MAIL: "changelogs@cm-ss13.com"

.dind:
  image: docker:20.10.2
  services:
    - docker:20.10.2-dind
  before_script:
    - "[ $CI_REGISTRY ] && command -v docker && echo $CI_JOB_TOKEN | docker login --username gitlab-ci-token --password-stdin $CI_REGISTRY"

# Prepare map files
build:map-templates:
  stage: prepare
  image: $IMG_BUILD_PYTHON
  dependencies: []
  script:
    - python3 tools/ci/template_dm_generator.py
  artifacts:
    name: 'map_template'
    paths:
      - 'maps/templates.dm'
    expire_in: '2 hours'

# TGUI Interface
build:tgui:
  stage: prepare
  image: $IMG_BUILD_NODE
  dependencies: []
  script:
    - chmod u+x tgui/bin/tgui
    - tgui/bin/tgui --ci
  artifacts:
    name: 'tgui_bundle'
    paths:
      - 'tgui/public/'
    expose_as: 'TGUI Bundles'
    expire_in: '3 days'

# Updated changelog for inclusion in game build if on main branch
# TODO Add support for auto-detecting MR changelog and including it, for Testmerges
report:changelog:
  stage: prepare
  image: $IMG_BUILD_PYTHON
  dependencies: []
  script:
    - pip install python-dateutil requests beautifulsoup4 pyyaml
    - GITLAB_CHANGELOG_PID=${CI_PROJECT_ID} python3 tools/changelogs/generate_changelogs.py dev html/changelogs/
    - python3 tools/GenerateChangelog/ss13_genchangelog.py html/changelog.html html/changelogs/
  artifacts:
    name: 'changelogs'
    paths:
      - 'html/changelog.html'
      - 'html/changelog.css'
      - 'html/changelogs/.all_changelog.yml'
    expose_as: 'Changelogs'
    expire_in: '3 days'
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

# Run DreamMaker to build game
build:dm:
  stage: build
  image: $IMG_BUILD_CMBASE
  dependencies:
    - 'build:tgui'
    - 'report:changelog'
  cache:
    key: "byond-${BYOND_MAJOR}.${BYOND_MINOR}-${CI_COMMIT_REF_SLUG}"
    paths:
      - byond.zip
  script:
    - DEBIAN_FRONTEND=noninteractive apt-get update && apt-get install -y build-essential libssl-dev unzip wget
    - "[ -f byond.zip ] || wget -O byond.zip $BYOND_DOWNLOAD_URL"
    - mkdir -p $BYOND_INSTALL_DIR
    - unzip byond.zip -d $BYOND_INSTALL_DIR && cd ${BYOND_INSTALL_DIR}/byond
    - make here
    - source bin/byondsetup
    - cd $CI_PROJECT_DIR
    - DreamMaker ${DM_PROJECT_NAME}.dme
  artifacts:
    name: 'binaries'
    paths:
      - "${DM_PROJECT_NAME}.dmb"
      - "${DM_PROJECT_NAME}.rsc"
    expose_as: 'DM Binaries'
    expire_in: '7 days'

# Run tests
test:json:
  stage: build
  image: $IMG_BUILD_PYTHON
  dependencies: []
  script:
    - find . -iname "*.json" -not -path "*/node_modules/*" -exec python3 ./tools/json_verifier.py {} \;

test:dreamchecker:
  stage: build
  dependencies:
    - 'build:map-templates'
  cache:
    key: "dreamchecker-${SPDMM_VERSION}-${CI_COMMIT_REF_SLUG}"
    paths:
      - dreamchecker
  script:
    - "[ -f dreamchecker ] || wget -O dreamchecker ${SPDMM_DOWNLOAD_URL}/dreamchecker"
    - chmod u+x dreamchecker
    - ./dreamchecker --json

# Build and push deployment container
build:runner:
  stage: extra
  extends: .dind
  dependencies:
    - 'build:dm'
    - 'build:map-templates'
    - 'build:tgui'
    - 'report:changelog'
  script:
    - docker pull ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG} || true
    - docker build
      --platform=linux/386
      --cache-from ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}
      --build-arg BUILDKIT_INLINE_CACHE=1
      --build-arg BYOND_BASE_IMAGE=${IMG_BUILD_CMBASE}
      --build-arg BYOND_MAJOR
      --build-arg BYOND_MINOR
      --build-arg BYOND_DOWNLOAD_URL
      --build-arg DM_PROJECT_NAME
      --tag ${CI_REGISTRY_IMAGE}:${CI_PIPELINE_ID}
      --tag ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}
      --target cm-runner .
    - docker push ${CI_REGISTRY_IMAGE}:${CI_PIPELINE_ID}
    - docker push ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG} || true
  rules:
    - if: '$CI_REGISTRY_IMAGE'

# Generate DM documentation
report:dmdoc:
  stage: extra
  dependencies:
    - 'build:map-templates'
  cache:
    key: "dmdoc-${SPDMM_VERSION}-${CI_COMMIT_REF_SLUG}"
    paths:
      - dmdoc-bin
  script:
    - "[ -f dmdoc-bin ] || wget -O dmdoc-bin ${SPDMM_DOWNLOAD_URL}/dmdoc"
    - chmod u+x dmdoc-bin
    - ./dmdoc-bin
  artifacts:
    name: ci_dmdoc
    paths:
      - 'dmdoc/'
    expose_as: 'DM Documentation'
    expire_in: '3 days'

report:maps:
  stage: extra
  dependencies: []
  cache:
    key: "dmm-tools-${SPDMM_VERSION}-${CI_COMMIT_REF_SLUG}"
    paths:
      - dmm-tools
  script:
    - "[ -f dmm-tools ] || wget -O dmm-tools ${SPDMM_DOWNLOAD_URL}/dmm-tools"
    - chmod u+x dmm-tools
    - find maps -iname "*.dmm" -exec dmm-tools map-info --json {} \; | tee mapinfo.txt
  artifacts:
    name: ci_mapinfo
    paths:
      - 'mapinfo.txt'
    expose_as: 'Map Info'
    expire_in: '7 days'
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == "push"'
      changes:
        - "maps/**/*.dmm"

report:map-renders:
  stage: extra
  dependencies: []
  cache:
    key: "dmm-tools-${SPDMM_VERSION}-${CI_COMMIT_REF_SLUG}"
    paths:
      - dmm-tools
  script:
    - "[ -f dmm-tools ] || wget -O dmm-tools ${SPDMM_DOWNLOAD_URL}/dmm-tools"
    - chmod u+x dmm-tools
    - mkdir -p map-renders
    - find maps/map_files -iname "*.dmm" -exec dmm-tools minimap -o maps-renders {} \;
  artifacts:
    name: ci_map_renders
    paths:
      - 'maps-renders/'
    expose_as: 'Maps Renders'
    expire_in: '1 day'
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == "push"'
      changes:
        - "maps/**/*.dmm"

commit:changelog:
  stage: extra
  image: $IMG_BUILD_NODE
  interruptible: false
  script:
    - eval `ssh-agent -s`
    - echo "$CM_CHANGELOG_DEPLOY_KEY" | tr -d '\r' | ssh-add - > /dev/null
    - git config --local user.email ${CM_CHANGELOG_DEPLOY_MAIL}
    - git config --local user.name ${CM_CHANGELOG_DEPLOY_NAME}
    - git config --local push.default simple
    - git add html/changelog*
    - git commit -m "changelog-${CI_COMMIT_BRANCH}"
    - git remote set-url origin "git@gitlab.com:${CI_PROJECT_PATH}.git"
    - GIT_SSH_COMMAND="ssh -o StrictHostKeyChecking=no" git push -o ci.skip origin HEAD:${CI_COMMIT_BRANCH}
  rules:
    - if: '$CM_CHANGELOG_DEPLOY_KEY && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == "push"'
  dependencies:
    - report:changelog
